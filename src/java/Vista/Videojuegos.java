/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

/**
 *
 * @author Naozen
 */
public class Videojuegos {
    private int idVideojuego;
    private String nombreVideojuego;
    private String genero;
    private String Plataforma;
    private int Rut_Cliente;

    public int getIdVideojuego() {
        return idVideojuego;
    }

    public void setIdVideojuego(int idVideojuego) {
        this.idVideojuego = idVideojuego;
    }

    public String getNombreVideojuego() {
        return nombreVideojuego;
    }

    public void setNombreVideojuego(String nombreVideojuego) {
        this.nombreVideojuego = nombreVideojuego;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getPlataforma() {
        return Plataforma;
    }

    public void setPlataforma(String Plataforma) {
        this.Plataforma = Plataforma;
    }

    public int getRut_Cliente() {
        return Rut_Cliente;
    }

    public void setRut_Cliente(int Rut_Cliente) {
        this.Rut_Cliente = Rut_Cliente;
    }

    @Override
    public String toString() {
        return "Videojuegos{" + "idVideojuego=" + idVideojuego + ", nombreVideojuego=" + nombreVideojuego + ", genero=" + genero + ", Plataforma=" + Plataforma + ", Rut_Cliente=" + Rut_Cliente + '}';
    }
}
