package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Naozen
 */
public class Conexion {
    private String username = "root";
    private String password = "root";
    private String host = "localhost";
    private String port = "3306";
    private String database = "videojuegos";
    private String classname = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://"+host+":"+port+"/"+database;
    private Connection conn;
    
    public Conexion() throws SQLException{
        try{ Class.forName(classname);
            conn = DriverManager.getConnection(url,username,password);
        }catch(ClassNotFoundException e){
            System.out.println("Error"+e);
            }catch(SQLException e){
            System.out.println("ERROR"+e);
        }
    }
    public Connection getConexion(){
        return conn;
}
    public static void main (String [] args) throws SQLException{
            Conexion conn = new Conexion();
    }
}
