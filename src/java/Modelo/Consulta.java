/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Naozen
 */
public class Consulta extends Conexion{

    public Consulta() throws SQLException {
    }
    public boolean autenticacion(String nickUsuario, String contraseña){
        PreparedStatement pst = null;
        ResultSet rs = null;
        try{
            String Consulta = "select * from usuario where nickUsuario = ? and contraseña =?";
            pst = getConexion().prepareStatement(Consulta);
            pst.setString(1, nickUsuario);
            pst.setString(2, contraseña);
            rs  = pst.executeQuery();
            if(rs.absolute(1)){
                return true;
            }                              
        }catch(SQLException e){
            System.out.println("Error"+e);
        }finally{
            try{
                if(getConexion() !=null){
                    getConexion().close();
                if(pst != null) pst.close();
                if(rs != null) rs.close();
                }
            }catch(Exception e){
                System.out.println("Error"+e);
            }
        }
        return false;
    }
    public boolean registrar(String nickUsuario, String email, String contraseña){
        PreparedStatement pst = null;
        
        try{
            String consulta = "insert into usuario (nickUsuario, email, contraseña) values(?,?,?)";
            pst = getConexion().prepareStatement(consulta);
            pst.setString(1, nickUsuario);
            pst.setString(2, email);
            pst.setString(3, contraseña);
            if(pst.executeUpdate() == 1){
                return true;
            }
        }catch(Exception e){
            System.out.println("Error"+e);
        }finally{
            try{
                if(getConexion() != null) getConexion().close();
                if(pst != null)pst.close();
            }catch(Exception e){
                System.out.println("Error"+e);
            }
        }
        return false;
    }
 }